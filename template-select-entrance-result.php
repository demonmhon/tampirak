<?php
    $sub_cat;
    $cat_entrance_id     = get_category_by_slug(THEME_CONFIG_CAT_ENTRANCE)->cat_ID;
    $cat_entrance_result = get_categories( array( 'child_of' => $cat_entrance_id, 'hide_empty' => 0 ) );
    $cat;
    if (is_single()):
        $category = get_the_category();
        $cat      = $category[0]->cat_ID;
    endif;
?>
<?php if (count($cat_entrance_result) > 0): ?>
<div class="row">
    <div class="col-xs-12 article-tools">
        <div id="entrance-result-select" class="form-group form-inline">
            <label for="select-entrance-result">ประกาศผลการสอบเข้าประถมศึกษาปีที่ 1 โรงเรียน</label>
            <select id="select-entrance-result" class="form-control">
                <option value="<?php echo get_category_link($cat_entrance_id); ?>"> </option>
                <?php foreach ($cat_entrance_result as $item): ?>
                <?php
                    $current_cat = 0;
                    if ($cat == $item->cat_ID):
                        $current_cat = 1;
                    endif;
                ?>
                <option value="<?php echo get_category_link($item->cat_ID); ?>"<?php if ($current_cat): ?> selected="selected"<?php endif; ?>><?php echo $item->cat_name; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
</div>
<?php endif; ?>
