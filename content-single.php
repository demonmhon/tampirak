<div class="row">
    <article id="post-<?php the_ID(); ?>" <?php post_class('article-content'); ?>>
        <header class="entry-header">
            <h1 class="title"> <?php the_title(); ?></h1>
            <span class="date"><?php the_time(THEME_CONFIG_FORMAT_DATE) ?></span>
        </header>

        <div class="entry-content">
            <?php the_content(); ?>
            <?php
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'tampirak' ),
                    'after'  => '</div>',
                ) );
            ?>
        </div>
    </article>
</div>
