<?php get_header(); ?>

    <?php
        $category         = get_the_category();
        $category_id      = $category[0]->cat_ID;
        $parent_cat_slug  = '';
        if ($category_id):
            if (get_category($category_id)->parent != 0):
                $parent_cat_slug = get_category(get_category($category_id)->parent)->slug;
            endif;
        endif;
    ?>

    <div id="primary" class="content-area row">
        <main id="main" class="post-entry-main col-md-9" role="main">
        <?php if ($parent_cat_slug == THEME_CONFIG_CAT_ENTRANCE) : ?>
            <?php get_template_part('template-select-entrance-result'); ?>
        <?php endif; ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'content', 'single' ); ?>
        <?php endwhile; ?>
        </main>
        <?php get_sidebar(); ?>
    </div>

<?php get_footer(); ?>
