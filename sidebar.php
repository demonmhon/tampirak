<div class="sidebar col-md-3">
    <?php if (in_category(THEME_CONFIG_CAT_NEWS)) : ?>
    <!-- Category News Archives -->
    <div class="row">
        <div class="col-entry-list col-xs-12">
            <div class="title-bar">
                <h2 class="title"><i class="icon fa fa-comment-o"></i>ข่าวสารย้อนหลัง</h2>
            </div>
            <div class="entry-content">
                <?php $archives = query_posts( array( 'category_name' => THEME_CONFIG_CAT_NEWS, 'posts_per_page' => THEME_CONFIG_LIMIT_ARCHIVES ) ); ?>
                <?php if (have_posts()): ?>
                    <ul class="list-unstyled">
                        <?php while (have_posts()): the_post(); ?>
                            <li class="post">
                                <span class="date"><?php the_time(THEME_CONFIG_FORMAT_DATE) ?></span>
                                <div class="news-content post-content"><a href="<?php the_permalink() ?>"><?php the_content('') ?></a></div>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <?php if (get_category($cat)->parent != 0 && !is_category(THEME_CONFIG_CAT_ENTRANCE)) : ?>
    <!-- Category Entrace Archives -->
    <div class="row">
        <div class="col-entry-list col-xs-12">
            <div class="title-bar">
                <h2 class="title"><i class="icon fa fa-list-ul"></i>ผลการสอบเข้า / ข้อมูลปี</h2>
            </div>
            <div class="entry-content">
                <?php $entrance_result = query_posts( array( 'cat' => $cat ) ); ?>
                <?php if (have_posts()): ?>
                <ul class="list-unstyled">
                    <?php while (have_posts()): the_post(); ?>
                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                    <?php endwhile; ?>
                </ul>
                <?php else: ?>
                    <p>ไม่มีข้อมูล</p>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <?php if ( is_active_sidebar( 'sidebar-1' ) ) :  ?>
    <div class="row">
        <div class="col-xs-12">
            <div id="secondary" class="widget-area" role="complementary">
                <?php dynamic_sidebar( 'sidebar-1' ); ?>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>
