<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <header id="site-header" class="site-header header">
        <div class="site-header-inner">
            <nav class="navbar" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#primary-nav" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="navbar-brand">
                           <span class="logo"><img src="<?php echo get_template_directory_uri() ?>/assets/img/main-logo-xs.png" alt="Brand Logo" class="img-responsive"></span><span class="text"><?php bloginfo( 'name' ); ?></span>
                        </a>
                    </div>
                </div>
                <nav id="primary-nav" class="primary collapse navbar-collapse">
                    <div class="container">
                        <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'menu_class' => 'nav navbar-nav', 'walker' => new wp_bootstrap_navwalker() ) ); ?>
                    </div>
                </nav>
            </nav>
        </div>
    </header>
    <div id="page-content" class="page-content">
        <div class="container">
            <div id="main-site-layout" class="main-site-layout">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="hero">
                            <div class="description">
                                <span class="site-description-text"><?php bloginfo( 'description' ); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
