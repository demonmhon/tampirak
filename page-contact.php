<?php get_header(); ?>

    <div id="primary" class="content-area row">
        <main id="main" class="post-entry-main col-xs-12" role="main">
        <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'content', 'page' ); ?>
            <div id="map" class=" article-content">
                <div class="embed-responsive embed-responsive-4by3">
                    <?php dynamic_sidebar( 'sidebar-contact-embed-map' ); ?>
                </div>
            </div>
        <?php endwhile; ?>
        </main>
        <?php get_sidebar(); ?>
    </div>

<?php get_footer(); ?>
