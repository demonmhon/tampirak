                </div>
            </div>
        </div>
    </div>
    <footer id="site-footer" class="site-footer footer">
        <div class="container">
            <div class="row  site-info">
                <div class="col-xs-12">
                    <p class="site-title"><?php bloginfo('title'); ?></p>
                </div>
            </div>
            <div class="row  address">
                <div class="col-xs-12">
                    <?php dynamic_sidebar( 'sidebar-contact-address' ); ?>
                    <?php dynamic_sidebar( 'sidebar-contact-phone' ); ?>
                </div>
            </div>
            <div class="row  copy-right">
                <div class="col-xs-12">
                    <p>&copy; <?php echo date("Y") ?> <a href="<?php bloginfo('url'); ?>"><?php bloginfo('url'); ?></a></p>
                </div>
            </div>
            <div class="row facebook-like">
                <div class="col-xs-12">
                    <div class="fb-like" data-href="https://www.facebook.com/pages/%E0%B8%AD%E0%B8%99%E0%B8%B8%E0%B8%9A%E0%B8%B2%E0%B8%A5%E0%B8%98%E0%B8%A3%E0%B8%A3%E0%B8%A1%E0%B8%A0%E0%B8%B4%E0%B8%A3%E0%B8%B1%E0%B8%81%E0%B8%A9%E0%B9%8C%E0%B8%98%E0%B8%99%E0%B8%9A%E0%B8%B8%E0%B8%A3%E0%B8%B5/165769933458829" data-width="280" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
                </div>
            </div>
        </div>
    </footer>
<?php wp_footer(); ?>
</body>
</html>
