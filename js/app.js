var app;

(function($) {
    var App = function () {
        return this.init();
    };

    App.prototype.init = function () {
        var self = this;

        self.setUI();

        return self;
    };

    App.prototype.setUI = function () {
        var self = this;

        self.setUISelectBox();
        self.setUITable();
        self.setUIImg();
        self.setContactFormUI();

        return self;
    };

    App.prototype.setUISelectBox = function () {
        var self = this;

        var selectBoxEntrance = $('#select-entrance-result');
        if (selectBoxEntrance) {
            selectBoxEntrance.selectpicker();
            selectBoxEntrance.on('change', function(){
                var entrance = $('#select-entrance-result').val();
                if (entrance != '') {
                    window.location = entrance;
                }
            });
        }

        return self;
    }

    App.prototype.setUITable = function () {
        var self = this;

        var tables = $('.article-content table');
        $.each(tables, function(i, table) {
            $(table).addClass('table');
        });

        return self;
    }

    App.prototype.setUIImg = function () {
        var self = this;

        var imgs = $('.article-content img');
        $.each(imgs, function(i, img) {
            $(img).addClass('responsive');
        });

        return self;
    }

    App.prototype.setContactFormUI = function () {
        var self = this;

        var contactForm = $('.contact-form');
        contactForm.find('input[type="text"]').addClass('form-control');
        contactForm.find('input[type="email"]').addClass('form-control');
        contactForm.find('textarea').addClass('form-control');
        contactForm.find('input[type="submit"]').addClass('btn btn-default');

        return self;
    }

    $(document).ready( function () { app = new App(); } );
})(jQuery);
