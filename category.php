<?php get_header(); ?>

    <?php $parent_cat_slug = get_category(get_category($cat)->parent) || ''; ?>
    <?php
        $parent_cat_slug = '';
        if (get_category($cat)->parent != 0) :
            $parent_cat_slug = get_category(get_category($cat)->parent)->slug;
        endif;
    ?>
    <?php if (is_category(THEME_CONFIG_CAT_ENTRANCE) || $parent_cat_slug == THEME_CONFIG_CAT_ENTRANCE): ?>
        <?php get_template_part('category-template-entrance'); ?>
    <?php else: ?>
        <div id="primary" class="content-area row">
            <main id="main" class="post-entry-main col-md-9" role="main">
                <div class="row">
                    <div class="category-title-bar col-xs-12">
                        <h1 class="title"><?php single_cat_title(); ?></h1>
                    </div>
                </div>
                <div class="row">
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php get_template_part( 'content', 'list' ); ?>
                    <?php endwhile; ?>
                </div>
            </main>
            <?php get_sidebar(); ?>
        </div>
    <?php endif ?>

<?php get_footer(); ?>
