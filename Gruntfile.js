module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // running `grunt uglify` will compile once
        uglify: {
            dev: {
                options: {
                    preserveComments: false,
                    mangle: false,
                    banner: '/*!' + "\n"
                        + ' * <%= pkg.name %> v<%= pkg.version %> (<%= pkg.homepage %>)' + "\n"
                        + ' * <%= pkg.author.name %> <%= pkg.author.email %> <%= pkg.author.homepage %>' + "\n"
                        + ' * <%= grunt.template.today("mmm dd, yyyy") %>' + "\n"
                        + " */ \n",
                    // sourceMap: function(path) { return path.replace(/.js/,'.map')}
                },
                files: {
                    './assets/app.min.js': [
                        // './js/jquery.min.js',
                        './js/bootstrap.min.js',
                        './js/bootstrap-select.min.js',
                        './js/app.js'
                    ]
                }
            }
        },

        // running `grunt less` will compile once
        less: {
            dev: {
                options: {
                    paths: ['./css'],
                    compress: true,
                    banner: '/*!' + "\n"
                        + ' * <%= pkg.name %> v<%= pkg.version %> (<%= pkg.homepage %>)' + "\n"
                        + ' * <%= pkg.author.name %> <%= pkg.author.email %> <%= pkg.author.homepage %>' + "\n"
                        + ' * <%= grunt.template.today("mmm dd, yyyy") %>' + "\n"
                        + " */ \n",
                },
                files: {
                    './assets/style.min.css': [
                        './css/bootstrap.css',
                        './css/bootstrap-select.css',
                        './css/font-awesome.css',
                        './less/style.less'
                    ]
                }
            }
        },

        // running `grunt watch` will watch for changes
        watch: {
            scripts: {
                files: ['./js/*.js'],
                tasks: ['uglify']
            },
            css: {
                files: ['./less/*.less'],
                tasks: ['less']
            }
        }
    });

    // Load the plugin that provides the tasks.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s)
    grunt.registerTask('default', ['uglify', 'less']);
};
