<?php get_header(); ?>
    <div class="row">
        <!-- Home content -->
        <div id="primary" class="col-intro col-xs-12 col-md-6">
            <?php while ( have_posts() ) : the_post(); ?>
            <!-- Intro -->
            <div class="intro-box">
                <div class="content-box row">
                    <div class="col-xs-12">
                        <div class="entry-content">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <div class="img-wrapper">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/home/focus-intro.png" alt="Intro image" class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="title-bar">
                            <h1 class="title"><?php the_title(); ?></h1>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
             <?php
                $activities = query_posts(
                    array(
                        category_name  => THEME_CONFIG_CAT_ACTIVITIES,
                        posts_per_page => 2,
                        orderby        => 'rand'
                    )
                );
            ?>
            <?php if (have_posts()): ?>
            <!-- Activities -->
            <div class="activities-box">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="title-bar">
                            <h1 class="title">กิจกรรม</h1>
                        </div>
                    </div>
                </div>
                <div class="content-box row">
                    <div class="col-xs-12 content-list">
                        <?php while (have_posts()): the_post(); ?>
                        <div class="entry-content">
                            <div class="post row">
                                <div class="col-xs-4 col-sm-3 activity-img">
                                    <div class="img-wrapper">
                                        <?php if ( has_post_thumbnail() ) : ?>
                                            <?php the_post_thumbnail('thumbnail', array( 'class' => "img-responsive attachment-$size" ) ); ?>
                                        <?php else: ?>
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/sample-256.jpg" class="img-responsive">
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-xs-8 col-sm-9 activity-entry">
                                    <h2 class="activity-title"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h2>
                                    <div class="news-content"><?php the_excerpt() ?></div>
                                </div>
                            </div>
                        </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <!-- News -->
        <div class="col-news col-xs-12 col-md-6">
            <aside>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="title-bar">
                            <h1 class="title"><i class="icon fa fa-comment-o"></i>ข่าวสารจากทางโรงเรียน</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 content-list">
                        <div class="entry-content">
                            <?php
                                $news_list = query_posts(
                                    array(
                                        category_name  => THEME_CONFIG_CAT_NEWS,
                                        posts_per_page => THEME_CONFIG_LIMIT_CAT_NEWS
                                    )
                                );
                            ?>
                            <?php if (have_posts()): ?>
                            <ul class="list-unstyled">
                                <?php while (have_posts()): the_post(); ?>
                                    <li class="post">
                                        <span class="date"><a href="<?php the_permalink(); ?>"><?php the_time(THEME_CONFIG_FORMAT_DATE) ?></a></span>
                                        <div class="news-content"><a href="<?php the_permalink(); ?>"><?php the_excerpt() ?></a></div>
                                    </li>
                                <?php endwhile; ?>
                                <li class="post">
                                    <div class="">
                                        <?php $category = get_category_by_slug( THEME_CONFIG_CAT_NEWS ); ?>
                                        <a href="<?php echo get_category_link( $category->cat_ID ) ?>" class="more-content-list">ดูทั้งหมด</a>
                                    </div>
                                </li>
                            </ul>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </aside>
        </div>
    </div>
<?php get_footer(); ?>
