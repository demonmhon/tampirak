<?php get_header(); ?>

    <div id="primary" class="content-area row">
        <main id="main" class="post-entry-main col-xs-12" role="main">
        <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'content', 'page' ); ?>
        <?php endwhile; ?>
        </main>
        <?php get_sidebar(); ?>
    </div>

<?php get_footer(); ?>
