<article id="post-<?php the_ID(); ?>" <?php post_class('article-excerpt'); ?>>
    <div class="row">
        <div class="col-xs-12 col-sm-2 feature-img">
            <?php if ( has_post_thumbnail() ) : ?>
                <?php the_post_thumbnail('medium'); ?>
            <?php else: ?>
                <img src="<?php bloginfo('template_url'); ?>/assets/img/sample-256.jpg" class="img-responsive">
            <?php endif; ?>
        </div>
        <div class="col-xs-12 col-sm-10">
            <header class="entry-header">
                <h1 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
            </header>
            <div class="entry-excerpt">
                <?php the_excerpt(); ?>
                <div class="more"><a href="<?php the_permalink(); ?>">อ่านต่อ</a></div>
            </div>
        </div>
    </div>
</article>
