<div id="primary" class="content-area row">
    <main id="main" class="post-entry-main col-md-9" role="main">
        <div class="row">
            <div class="category-title-bar col-xs-12">
                <h1 class="title"><small>ผลการสอบเข้าประถม</small><?php single_cat_title(); ?></h1>
            </div>
        </div>
        <?php get_template_part('template-select-entrance-result'); ?>
        <?php if (!is_category(THEME_CONFIG_CAT_ENTRANCE)): ?>
            <?php if (have_posts()): ?>
                <?php $count = 0; ?>
                <?php while (have_posts() && $count < 1): the_post(); ?>
                    <?php get_template_part( 'content', 'single' ); ?>
                    <?php $count++; ?>
                <?php endwhile; ?>
            <?php else: ?>
                <div class="row">
                    <article class="article-content">
                        <p>ไม่มีข้อมูล</p>
                    </article>
                </div>
            <?php endif; ?>
        <?php else: ?>
            <?php while (have_posts()): the_post(); ?>
            <?php
                $cats = get_the_category();
                $cat_slug = $cats[0]->slug;
            ?>
            <?php if ($cat_slug == THEME_CONFIG_CAT_ENTRANCE): ?>
                <?php get_template_part( 'content', 'single' ); ?>
            <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>
    </main>
    <?php get_sidebar(); ?>
</div>
